use rand::prelude::*;
use writeppm::{Color, PPM};

// Overall goal: Implement some morphological transform with different structuring elements
// from scratch. Chosen transform: Erosion

// 1. Create test data. This should be an image with some circles of random size and position.
// 2. Implement erosion with a structuring element of size 3x3 and all ones.
// 3. Implement erosion with an arbitrary structuring element.
//
//
//

fn random_image(width: u32, height: u32) -> PPM {
    let mut image = PPM::create_zeroed(width, height, 255);
    let mut rng = rand::thread_rng();

    for _ in 0..100 {
        let x = rng.gen_range(0..width as i32);
        let y = rng.gen_range(0..height as i32);
        let radius = rng.gen_range(0..10);
        for ix in (x - radius)..(x + radius) {
            for iy in (y - radius)..(y + radius) {
                if ix < 0 || ix >= width as i32 || iy < 0 || iy >= height as i32 {
                    continue;
                }
                let dx = ix - x;
                let dy = iy - y;
                let dist2 = dx * dx + dy * dy;
                if dist2 < radius * radius {
                    image.set_pixel(ix as u32, iy as u32, Color::new(1.0, 1.0, 1.0));
                }
            }
        }
    }

    image
}

fn main() {
    let image = random_image(256, 256);
    image.write("test.ppm");
}
